<?php defined('ACCESSIBLE') or die('No direct script access.');


function getAllSong($database) {
    $sql = 'SELECT * FROM musics';
    return query($database, $sql);
}


function countSong($database) {
    $sql = 'SELECT count(id) AS total FROM musics';
    return queryOne($database, $sql);
}


function SongExist($database, $name) {
    $sql = 'SELECT count(id) AS total FROM musics WHERE name = :name';
    $total = queryOne($database, $sql, array('name' => $name));
    if($total['total'] > 0){
        return true;
    }else{
        return false;
    }
}

function createSong($database, $titre,$artiste,$genre, $annee)
{
    $sql = 'INSERT INTO film (titre,artiste,genre,annee)
            VALUES (:titre,:artiste,:genre,:annee)';
    $parameters = array(
        'titre' => $titre,
        'artiste' => $artiste,
        'genre' => $genre,
        'annee' => $annee,
    );
    queryNoResult($database, $sql, $parameters);
}

