<?php
/*
 * INFOS
 *  - Ensemble de fonctions permettant de gérer les comptes utilisateur
*/


/*
 * Gestion connexion compte utilisateur
 * IN  : usernameMail & password
 * OUT : - si connexion : return 0
 *       - sinon : return -1
*/
function connection_account($usernameMail, $password) {
    // Si connexion avec adresse mail
   
    $userMail = utilisateurs::factory('utilisateurs')->where('mail', $usernameMail)->find_one();

    if (!empty($userMail))
    {
        
        $pass = $userMail->password;
        if (password_verify($password, $pass))
        {
            $_SESSION['id'] = $userMail->id;
            $_SESSION['mail'] = $userMail->mail;
            $_SESSION['username'] = $userMail->username;
            $_SESSION['pays'] = $userMail->pays;
            
            $_SESSION['age'] = $userMail->age;
            $_SESSION['user_connect'] = "Yes";
            return 0;
        }
        return -1;
    }
    // Si connexion avec username
    $userUsername = utilisateurs::factory('utilisateurs')->where('username', $usernameMail)->find_one();
    if (!empty($userUsername))
    {
    
        $pass = $userUsername->password;
        if (password_verify($password, $pass))
        {
            
            $_SESSION['id'] = $userUsername->id;
            $_SESSION['mail'] = $userUsername->mail;
            $_SESSION['username'] = $userUsername->username;
            $_SESSION['pays'] = $userUsername->pays;
            $_SESSION['age'] = $userUsername->age;
            $_SESSION['playlist'] = $userUsername->playlist;
            $_SESSION['user_connect'] = "Yes";
            
            return 0;
        }
        return -1;
    }
    $_SESSION['user_connect'] = "No";
    
    return -1;
}

/*
 * Gestion création compte utilisateur
 * IN  : - username & mail & password
 * OUT : - si création du compte : return 0
 *       - sinon : return -1
*/
function creation_account($username, $adresseMail, $password) {
    // On commence par regarder si l'username et le mail sont disponibles
    $_Mail = utilisateurs::factory('utilisateurs')->where('mail', $adresseMail)->find_one();
    $_Username = utilisateurs::factory('utilisateurs')->where('username', $username)->find_one();
    // Si disponible alors création du compte
    if (empty($_Mail) && empty($_Username))
    {
        $user = Model::factory('utilisateurs')->create();
        $user->username = $username;
        $user->mail = $adresseMail;
        $user->password = password_hash($password, PASSWORD_DEFAULT);
        $user->activation = 0;
        $user->supprimer = 0;
        $user->save();
    }
}



function update_playlist($user_id, $playlist)
{
    $user = Model::factory('playlist')->find_one($user_id);
    
     $user = $playlist;

    $user->save();
}



?>