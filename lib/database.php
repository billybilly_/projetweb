<?php defined('ACCESSIBLE') or die('No direct script access.');

function getDatabaseConnexion(){
    try {
        $db = new PDO('sqlite:song.sqlite3');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
    }catch (Exception $e){
        exit('Erreur : '. $e->getMessage());
    }
}


function prepare_and_execute_query($database, $sql, $parameters){
    $query = $database->prepare($sql);
    if($parameters){
        $query->execute($parameters);
    }else{
        $query->execute();
    }
    return $query;
}


function query($database, $sql, $parameters=null){
    $query = prepare_and_execute_query($database, $sql, $parameters);
    $results = $query->fetchAll(PDO::FETCH_ASSOC);
    $query->closeCursor();
    return $results;
}

function queryNoResult($database, $sql, $parameters=null) {
    prepare_and_execute_query($database, $sql, $parameters);
}

function queryOne($database, $sql, $parameters=null){
    $query = prepare_and_execute_query($database, $sql, $parameters);
    $results = $query->fetch(PDO::FETCH_ASSOC);
    $query->closeCursor();
    return $results;
}
function getDataFromPost($arr_parameters)
{
    $data = array();
    foreach ($arr_parameters as $attr => $type) {
        if ($type == 'int') {
            $value = intval($_POST[$attr]);
        } else {
            $value = htmlspecialchars($_POST[$attr]);
        }
        $data[$attr] = $value;
    }
    return $data;
}

function checkEmpty($attr)
{
    if (empty($_POST[$attr])) {
        return true;
    }
    return false;
}

function checkRequiredFields($arr_parameters_required)
{
    $required_field_set = true;
    foreach ($arr_parameters_required as $attr) {
        if (checkEmpty($attr)) {
            $required_field_set = false;
        }
    }
    return $required_field_set;
}

function getValueFromPost($attr)
{
    if ($_POST and isset($_POST[$attr])) {
        return $_POST[$attr];
    }
    return "";
}

function treatmentFormSong($database, $arr_parameters, $arr_parameters_required)
{
    if ($_POST) {
        //Parameters from form
        $data = getDataFromPost($arr_parameters);

        if (!checkRequiredFields($arr_parameters_required)) {
            return 'Tous les champs doivent être renseigné !';
        }
        else{
            createSong($database, $data['titre'],$data['artiste'],$data['genre'], $data['annee']);
            return sprintf("Votre musique %s est ajoutée !", $data['titre']);
        }

        

    }

}

