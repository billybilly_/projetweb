<?php require 'vendor/autoload.php';?>
<?php require_once 'lib/const.php'; ?>
<?php require_once 'lib/database.php'; ?>
<?php require_once 'lib/song.php'; ?>
<?php require_once 'lib/models.php'; ?>
<?php require_once 'lib/functions_account.php' ?>
<?php use \Tamtamchik\SimpleFlash\Flash; ?>
<?php


$database=GetDatabaseconnexion();


$sql = 'CREATE TABLE IF NOT EXISTS "musics" (
    `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `titre` TEXT NOT NULL,
    `artiste` TEXT NOT NULL,
    `genre` INTEGER NOT NULL,
    `annee` INTEGER NOT NULL,
    FOREIGN KEY(`titre`) REFERENCES `titres`(`id`)
    FOREIGN KEY(`artiste`) REFERENCES `artistes`(`id`)
    FOREIGN KEY(`genre`) REFERENCES `genres`(`id`)
    FOREIGN KEY(`annee`) REFERENCES `annees`(`id`)
    );';

query($database, $sql);




$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::before('start', function(&$params, &$output){
    ORM::configure('sqlite:song.sqlite3');
});



Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); 
    $twig->addGlobal('session', $_SESSION);
});


Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

session_start();

Flight::route('/', function(){
    Flight::render('index.twig');
});

Flight::route('/random/', function(){
    $arr_ids = ORM::for_table('musics')->select('id')->find_array();
    $alea=rand(0, count($arr_ids));
    $id_alea = $arr_ids[$alea]["id"];
    $music = ORM::for_table('musics')->where('id', $id_alea)->find_many();
    $data=[
        'musics'=>$music
    ];
    Flight::render('random.twig',$data);
});

Flight::route('/top/', function(){
    $music = ORM::for_table('musics')->order_by_asc('id')->limit(100)->find_many();
    $data=[
        'musics'=>$music
    ];

    Flight::render('top.twig',$data);
});

Flight::route('/bibliotheque/@tri', function($tri){
    
        if ($tri=='classement') 
        {
            $music = ORM::for_table('musics')->order_by_asc('id')->find_many();;
            $data=[
                'musics'=>$music
            ];
            Flight::render('biblio.twig',$data);
        }
        if ($tri=='artiste') 
        {
            $music = ORM::for_table('musics')->order_by_asc('artiste')->find_many();;
            $data=[
                'musics'=>$music
            ];
            Flight::render('biblio.twig',$data);
        }
        if ($tri=='genre') 
        {
            $music = ORM::for_table('musics')->order_by_asc('genre')->find_many();;
            $data=[
                'musics'=>$music
            ];
            Flight::render('biblio.twig',$data);
        }
        if ($tri=='annee') 
        {
            $music = ORM::for_table('musics')->order_by_asc('annee')->find_many();;
            $data=[
                'musics'=>$music
            ];
            Flight::render('biblio.twig',$data);
        }

    
    
    
});

Flight::route('/playlist', function(){
    $music = ORM::for_table('playlist')->order_by_asc('id')->find_many();
    $data=[
        'playlist'=>$music
    ];
    
    Flight::render('playlist.twig',$data);
});

Flight::route('/playlist/delete/@id', function($id){
    $music = Model::factory('Playlis')->find_one($id);
    Flash::success(sprintf('Musique %s supprimée!', $music->titre));
    $music->delete();
    Flight::redirect('/playlist');
});

Flight::route('/form(/@id)', function($id){ 
    //1. Create a empty dinosaur
    if($id){
        $music = Model::factory('Music')->find_one($id);
    }else{
        $music = Model::factory('Music')->create();
        $music->titre = null;
        $music->artiste = null;
        $music->genre = null;
        $music->annee = null;
    }
    

    //2. if method is POST
    
    if(Flight::request()->method == 'POST'){
        $music->titre = Flight::request()->data->titre;
        $music->artiste = Flight::request()->data->artiste;
        $music->genre = Flight::request()->data->genre;
        $music->annee = Flight::request()->data->annee;
        
            $music->save();
            Flight::redirect('/bibliotheque/classement');
        }
        else{
            Flash::warning('Ooops some errors are in your form!');
            $data = [
                'music' => $music,
                
            ];
            Flight::render('/form.twig', $data);
        }  

    
});

Flight::route('/bibliotheque/delete/@id', function($id){
    $music = Model::factory('Music')->find_one($id);
    Flash::success(sprintf('Musique %s supprimée!', $music->titre));
    $music->delete();
    Flight::redirect('/bibliotheque/classement');
});

Flight::route('/bibliotheque/addplaylist/@id', function($id){

    $database=GetDatabaseconnexion();
$sql = "INSERT INTO playlist SELECT * FROM musics WHERE id='$id'";

query($database, $sql);
        

    Flight::redirect('/playlist');
    
});

Flight::route('/connexion/', function () {
    // Si connexion
    
    if (isset($_POST['btn_connexion']))
    {
        // Récupération des informations
        $usernameMail = $_POST['usernameMail'];
        $password = $_POST['passwordCo'];
        
        // Connexion
        connection_account($usernameMail, $password);
    }
    // Si création de compte
    if (isset($_POST['btn_inscription']))
    {
        // Récupération des informations
        $username = $_POST['username'];
        $adresseMail = $_POST['adresseMail'];
        $password = $_POST['passwordInsc'];
       
        // Création du compte
        if (creation_account($username, $adresseMail, $password) == 0)
        {
            // Connexion
            connection_account($username, $password);
        }
    }

    Flight::render('connexion.twig');
});



Flight::route('/deconnexion/', function () {
    $_SESSION['user_connect'] = "No"; //deconnecter l'utilisateur
    
 
    Flight::render('index.twig');
});




Flight::start();
